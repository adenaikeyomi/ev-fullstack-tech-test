class Company {
  constructor(name, email, companyFields) {
    this.name = name;
    this.email = email;
    this.companyFields = companyFields;
  }
  getName() {
    return this.name
  }
  getEmail() {
    return this.email
  }
  getCompanyFields() {
    return this.companyFields
  }
}