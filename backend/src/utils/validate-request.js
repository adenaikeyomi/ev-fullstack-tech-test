const BadRequestException = require("../exceptions/bad-request.exception");
const { validationResult } = require("express-validator")
module.exports = (cb) => {
  return (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return next(new BadRequestException(errors.array()));
    }
    cb(req, res, next)
  }
}
