const { connect } = require("mongoose");
class DatabaseManager {
  static async connect(str = process.env.DB_CONNECT_STRING) {
    connect(str).then(() => {
      console.log('Database successfully connected');
    }).catch(e => {
      console.error(e)
    })
  }
  static async clearDatabase() {
    // await Promise.all([mongoose.connection.collections[''].drop()]).then(x => { x }).catch(e => {
    //   console.error(e)
    // })
  }
  static async fillWithTestData() {

  }
  static async connectToDev() {
    await DatabaseManager.connect(
      'mongodb+srv://unamme:9hPOIlsYpMmwodqc@cluster0.a5bix.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'
    )
  }
}
module.exports = DatabaseManager;