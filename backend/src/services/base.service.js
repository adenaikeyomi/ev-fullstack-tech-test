const InternalServerException = require("../exceptions/internal-server-error.exception")
const CompanyInformationModel = require("../models/company-information.model");
class BaseService {
  constructor() { }

  async getAll(searchTerm) {
    try {
      console.log({ searchTerm })
      return await CompanyInformationModel.find({}).lean();
    } catch (error) {
      throw new InternalServerException(e)
    }
  }
  async get(id) {
    try {
      return await CompanyInformationModel.findById(id).lean();
    } catch (error) {
      throw new InternalServerException(e)
    }
  }

  async create(companyData) {
    try {
      const res = await new CompanyInformationModel(companyData).save()
      return res;
    } catch (e) {
      throw new InternalServerException(e)
    }
  }
  async update(id, partialCompany) {
    try {
      const company = await CompanyInformationModel.findByIdAndUpdate(id, partialCompany);
      return company;
    } catch (e) {
      throw new InternalServerException(e)
    }
  }
  async delete(id) {
    try {
      const res = await CompanyInformationModel.findByIdAndDelete(id);
      return !!res
    } catch (error) {
      throw new InternalServerException(e)
    }
  }

}

module.exports = BaseService