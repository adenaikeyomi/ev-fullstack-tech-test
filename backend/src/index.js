
require('dotenv').config()
const Application = require("./application");
const BaseRouter = require("./routers/base.router")
const BaseService = require("./services/base.service");
const BaseController = require("./controller/base.controller");
const DatabaseManager = require('./utils/database-manager');

const baseModule = new BaseRouter(new BaseController(new BaseService()))
DatabaseManager.connect()
module.exports = new Application([baseModule])
