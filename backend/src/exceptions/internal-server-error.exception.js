const BaseServerError = require("./baseServerError");
class InternalServerException extends BaseServerError {
  constructor(message) {
    super(message, 500);
    this.message = message;
    this.status = 500
  }
}
module.exports = InternalServerException

