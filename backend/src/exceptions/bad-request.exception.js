const BaseServerError = require("./baseServerError");

class BadRequestException extends BaseServerError {
  constructor(message) {
    super(message, 400)
    this.message = message;
    this.status = 400
  }
}
module.exports = BadRequestException;