const { Schema, model } = require("mongoose");
const s = new Schema({
  name: {
    type: String,
    required: true
  },
  createdDate: {
    type: Date,
    default: Date.now
  },
  email: {
    type: String,
    required: true
  },
  companyFields: {
    type: Object,
    required: true
  }
});

const CompanyInformationModel = model('company_information', s);
module.exports = CompanyInformationModel;
