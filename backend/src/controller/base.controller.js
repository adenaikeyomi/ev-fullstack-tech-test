class BaseController {
  constructor(baseService) {
    this.baseService = baseService
  }
  getAll(req, res, next) {
    this.baseService.getAll(req.query.search).then(x => {
      res.status(200).json(x)
    }).catch(next)
  }
  get(req, res, next) {
    this.baseService.get(req.query.id).then(x => {
      res.status(200).json(x)
    }).catch(next)
  }
  delete(req, res, next) {
    this.baseService.delete(req.query.id).then(x => {
      res.status(200).json(x)
    }).catch(next)
  }
  update(req, res, next) {
    this.baseService.update(req.body.id, req.body.update).then(x => {
      res.status(200).json(x)
    }).catch(next)
  }
  create(req, res, next) {
    this.baseService.create(req.body).then(x => {
      res.status(200).json(x)
    }).catch(e => {
      next(e);
    })
  }
}
module.exports = BaseController;