const { Router } = require("express")
const { body } = require('express-validator');
const { validate } = require("../models/company-information.model");
const validateRequest = require("../utils/validate-request");


class BaseRouter {
  constructor(baseController) {
    this.path = '/companies'
    this.router = Router()
    this.baseController = baseController;
    this.init()
  }
  init() {
    const validations = [
      body('name').exists(),
      body('email').exists().isEmail(),
      body('companyFields').exists(),
    ]
    this.router.get('', (req, res, next) => this.baseController.getAll(req, res, next))
    this.router.delete('', body('id').exists(), validateRequest((req, res, next) => this.baseController.delete(req, res, next)))
    this.router.put('', ...validations, validateRequest((req, res, next) => this.baseController.update(req, res, next)))
    this.router.post(
      '',
      ...validations,
      validateRequest(
        (req, res, next) => this.baseController.create(req, res, next)
      ),
    );
  }
}

module.exports = BaseRouter;