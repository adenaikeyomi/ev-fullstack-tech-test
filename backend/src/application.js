const express = require("express");
const cors = require("cors");
class Application {
  constructor(routes) {
    this.express = express()
    this.routes = routes;
    this.initMiddlewares();
    this.start()
  }
  initMiddlewares() {
    this.express.use(cors())
    this.express.use(express.json())
    this.express.use(express.urlencoded({ extended: false }))
  }
  start() {
    this.routes.forEach(x => {
      const path = `/v1${x.path}`
      this.express.use(path, x.router)
    })
    this.express.use((err, req, res, next) => {
      console.error(err)
      res.status(err.status).json({
        status: err.status,
        message: err.message
      })
    })
    this.express.listen(3002, () => {
      console.log("Server started on port 3001");
    })
  }
}



module.exports = Application