const DatabaseManager = require("../src/utils/database-manager")
const BaseService = require("../src/services/base.service");
const CompanyInformationModel = require("../src/models/company-information.model");
const baseService = new BaseService()
describe('Base service', () => {
  beforeAll(async () => {
    await DatabaseManager.connectToDev()
  })
  it('should create a company', async () => {
    expect.assertions(1);
    const res = await baseService.create({
      "name": "name",
      "email": "adenaikeyomi@gmail.com",
      "companyFields": {
        "population": 2000
      }
    })
    const company = await CompanyInformationModel.findById(res._id);
    expect(!!company).toBe(true);
  })
  it('should update a company', async () => {
    expect.assertions(1);
    await baseService.update({
      name: 'updatename'
    });
    const res = await CompanyInformationModel.findOne({
      name: 'updatename'
    });
    expect(res !== null).toBeTruthy()
  })

  it('should delete a company', async () => {

    expect.assertions(2);
    const { _id } = await CompanyInformationModel.findOne({ name: 'name' }, { _id: 1 }).lean();
    await baseService.delete(_id);
    expect(_id).toBeDefined()
    const endRes = await CompanyInformationModel.findById(_id).lean();
    expect(endRes).toBe(null)
  })

})