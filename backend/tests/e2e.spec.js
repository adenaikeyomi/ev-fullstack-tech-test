const request = require("supertest");
const application = require("../src/index");
const DatabaseManager = require("../src/utils/database-manager");
describe('end-to-end test', () => {
  beforeAll(() => {
    DatabaseManager.connectToDev()
  })
  it('should create a company', () => {
    request(application.express)
      .post("/v1/company")
      .send({
        name: 'test_company',
        email: 'test_company@email.com'
      }).then(res => {
        expect(!!res.body._id).toBeTruthy();
      })
  })
  it('should get all companies', () => {
    request(application.express)
      .get("/v1/company")
      .then(res => {
        expect(res.length > 0).toBeTruthy();
      })
  })

})
