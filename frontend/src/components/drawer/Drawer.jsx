import React, { useEffect } from "react";
import styles from "./styles.module.css";
export const Drawer = ({ title, children, subtitle, onClose, active }) => {
  useEffect(() => {
    document.onkeyup = (e) => {
      if (e.key === "escape") {
        onClose();
      }
    };
    return () => {
      document.onkeyup = null;
    };
  }, [onClose]);
  return (
    <div
      className={`${styles.drawer} ${active ? styles.open : styles.closed} `}
    >
      <header>
        <h2 className={styles.drawer_title}>{title}</h2>
        {subtitle && <p className={styles.drawer_subtitle}>{subtitle}</p>}
        <button className={styles.close_button} onClick={onClose}>
          x
        </button>
      </header>
      <div className={styles.children_container}>{children}</div>
    </div>
  );
};
