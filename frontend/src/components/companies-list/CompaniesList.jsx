import React, { memo } from "react";
import styles from "./styles.module.css";
export const CompaniesList = memo(
  ({ selectedCompanyId, companies, onClickCompany }) => {
    return (
      <ul className={styles.companies_list}>
        {companies.map((company) => {
          return (
            <li
              onClick={() => {
                onClickCompany(company);
              }}
              key={company._id}
              className={
                company._id === selectedCompanyId
                  ? styles.active_companies_list_item
                  : ""
              }
            >
              <h4>{company.name}</h4>
              <small>{company.email}</small>
              <br />
              <small>{new Date(company.createdDate).toUTCString()}</small>
            </li>
          );
        })}
      </ul>
    );
  }
);
