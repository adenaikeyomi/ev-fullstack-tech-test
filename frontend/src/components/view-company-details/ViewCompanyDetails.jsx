import React from "react";
import styles from "./styles.module.css";
export const ViewCompanyDetails = ({ companyData }) => {
  return (
    <div className={styles.view_company_container}>
      {Object.entries(companyData).map(([key, value]) => {
        return (
          <div key={key}>
            <p>
              {key}:{JSON.stringify(value)}
            </p>
          </div>
        );
      })}
    </div>
  );
};
