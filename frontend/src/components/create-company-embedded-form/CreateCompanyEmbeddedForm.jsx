import React from "react";
import styles from "./styles.module.css";
export const CreateCompanyEmbeddedForm = ({ onCreate }) => {
  const onSubmit = (e) => {
    e.preventDefault();
  };
  return (
    <form onSubmit={onSubmit}>
      {["name", "email", "companyDetails"].map((x) => {
        return <input key={x} name={x} type="text" placeholder={x} />;
      })}
    </form>
  );
};
