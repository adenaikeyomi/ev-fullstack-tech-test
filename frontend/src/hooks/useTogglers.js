import { useCallback, useState } from "react"

export const useTogglers = (defaults) => {
  const [toggles, setToggles] = useState(defaults);
  const toggle = useCallback((key) => {
    setToggles(s => {
      return {
        ...s,
        [key]: !s[key]
      }
    })
  }, [])
  const onToggleEventCallback = useCallback((key) => {
    return () => {
      toggle(key)
    }
  }, [toggle])
  const show = useCallback((key) => {
    setToggles(s => {
      return {
        ...s,
        [key]: true
      }
    })
  }, [])
  return {
    onToggle: toggle,
    onShow: show,
    onToggleEventCallback,
    toggles
  }
}