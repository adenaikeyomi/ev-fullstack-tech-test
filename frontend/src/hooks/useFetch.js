import { useCallback, useEffect, useState } from 'react'
export const useFetch = (url, options) => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState({});
  const [errors, setErrors] = useState([]);
  const [lastFetch, setLastFetch] = useState(null)
  useEffect(() => {
    if (url && options) {
      callFetch(url, options)
    }
  }, [url, options])
  const callFetch = useCallback((url, options) => {
    const _callFetch = (res, rej) => {
      fetch(`http://localhost:3002/v1${url}`, options)
        .then((x) => x.json())
        .then((x) => {
          if (x.status !== 200) {
            rej(x)
            setErrors(x)
          }
          setLastFetch(Date.now())
          setData(x);
          res(x);
        })
        .catch((err) => {
          setErrors(err);
          rej(err)
        })
        .finally(() => {
          setLoading(false);
        });
    }
    return new Promise((res, rej) => {
      setLoading(true);
      if (Date.now() - lastFetch > (5000 * 2)) {
        res(data)
      }
      _callFetch(res, rej)

    });
  }, []);
  return {
    callFetch,
    loading,
    data,
    errors,
  };
};