import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useFetch } from "./hooks/useFetch";
import { useTogglers } from "./hooks/useTogglers";
import { CompaniesList } from "./components/companies-list/CompaniesList";
import { Drawer } from "./components/drawer/Drawer";
import { Loader } from "./components/loader/Loader";
import { ViewCompanyDetails } from "./components/view-company-details/ViewCompanyDetails";
import { CreateCompanyEmbeddedForm } from "./components/create-company-embedded-form/CreateCompanyEmbeddedForm";
const App = () => {
  const { callFetch, data: companiesListItems, loading } = useFetch();
  const [selectedCompanyId, setSelectedCompanyId] = useState("");
  const [search, setSearch] = useState("");
  const [createdDate, setCreatedDate] = useState(null);
  const { toggles, onToggleEventCallback, onToggle, onShow } = useTogglers({
    createCompanyDrawer: false,
    readonlyViewCompany: false,
  });
  useEffect(() => {
    callFetch("/companies", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((x) => {
        console.log(x);
      })
      .catch((err) => {
        console.error(err);
      });
  }, []);
  const filteredCompanies = useMemo(() => {
    const s = search.toLowerCase();
    const result = Array.isArray(companiesListItems)
      ? companiesListItems.filter(
          (x) =>
            x.email.toLowerCase().includes(s) ||
            x.name.toLowerCase().includes(s)
        )
      : [];
    return result[0] ? result : companiesListItems;
  }, [companiesListItems, search]);
  const selectedCompanyData = useMemo(() => {
    return Array.isArray(companiesListItems) && selectedCompanyId
      ? companiesListItems.find((x) => x._id === selectedCompanyId)
      : {};
  }, [selectedCompanyId, companiesListItems]);
  const onClickCompany = useCallback(
    ({ _id }) => {
      setSelectedCompanyId(_id);
    },
    [setSelectedCompanyId, onShow]
  );
  const onCreateCompany = useCallback(
    (company) => {
      callFetch("/", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
      });
      console.log(companiesListItems);
    },
    [companiesListItems]
  );
  const createCompany = () => {
    console.log("Company");
  };
  const onClose = useCallback(() => {
    setSelectedCompanyId("");
    onShow("createCompanyDrawer");
  }, []);
  const hasSelectedCompanyData = !!Object.values(selectedCompanyData)[0];
  return (
    <main id="page_container">
      {loading && <Loader />}
      <button
        onClick={() => {
          onToggle("createCompanyDrawer");
          setSelectedCompanyId("");
        }}
      >
        Create company
      </button>
      {/* Companies List */}
      {Array.isArray(companiesListItems) && companiesListItems[0] && (
        <>
          <input
            onChange={(e) => {
              setSearch(e.currentTarget.value);
            }}
            type="text"
            name="search"
            placeholder="Search"
          />
          <input
            onChange={(e) => {
              setCreatedDate(e.currentTarget.value);
            }}
            type="date"
            name="date"
            placeholder="Date created"
          />
          <CompaniesList
            onClickCompany={onClickCompany}
            selectedCompanyId={selectedCompanyId}
            companies={filteredCompanies}
          />
        </>
      )}
      <Drawer
        active={hasSelectedCompanyData || toggles.createCompanyDrawer}
        title={hasSelectedCompanyData ? "View company" : "Create company"}
        onClose={onClose}
      >
        <>
          {toggles.createCompanyDrawer && (
            <CreateCompanyEmbeddedForm onCreate={onCreateCompany} />
          )}
          {selectedCompanyData && (
            <ViewCompanyDetails companyData={selectedCompanyData} />
          )}
        </>
      </Drawer>
    </main>
  );
};

export default App;
